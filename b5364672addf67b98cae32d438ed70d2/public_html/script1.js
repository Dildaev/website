let Table = document.getElementById('table')
let selectad = document.getElementById('admin')
let selectdi = document.getElementById('dist')
let selectty = document.getElementById('type')
let selectso = document.getElementById('socc sale')
let but = document.getElementById('but')
let pagination = document.querySelector('#pagination')

let notesOnPage = 10;


let data1;
let data;
let result;
let result1;
let result2;
let finalres;
function unique(arr) {//филттрует массив оставляя в нем тольок уникальные значенияия
	let result = [];
	
	for (let str of arr) {
		if (!result.includes(str)) {
			result.push(str);
		}
	}
	
	return result;
}

function getMessage(){
	let xhr = new XMLHttpRequest(); 
	
	xhr.open('GET', `http://exam-2020-1-api.std-400.ist.mospolytech.ru/api/data1`);
	xhr.send();
	
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			data = JSON.parse(xhr.responseText);
			data.sort(function(a,b){
				return b.rate-a.rate;
			});
			
			
			console.log(data);
			
			let district = JSON.parse(xhr.response).map(District => {return District.district;});//создаине json файла тольок из значений district
			let typeObject = JSON.parse(xhr.response).map(Type => {return Type.typeObject;});//создаине json файла тольок из значений type
			let admArea = JSON.parse(xhr.response).map(Area => {return Area.admArea;});//создаине json файла тольок из значений admArea
			(unique(admArea)).forEach(element => {//создание строки из json файла с уникальными элементами и добавление всех значений и получившейся строки в admin(select для админ округа)
				let admAreaOption = document.createElement('option');
				admAreaOption.innerHTML = `${String(element)}`;
				document.getElementById('admin').append(admAreaOption);
			});
			(unique(district)).forEach(element => {//создание строки из json файла с уникальными элементами и добавление всех значений и получившейся строки в district(select для района)
				let districtOption = document.createElement('option');
				districtOption.innerHTML = `${String(element)}`;
				document.getElementById('dist').append(districtOption);
				
			});
			(unique(typeObject)).forEach(element => {//создание строки из json файла с уникальными элементами и добавление всех значений и получившейся строки в type(select для типа)
				let typeObjectOption = document.createElement('option');
				typeObjectOption.innerHTML = `${String(element)}`;
				document.getElementById('type').append(typeObjectOption);
				
			});
			
			
			
		}
	}
}
getMessage();


function getSecMessage(){
	let xhr1 = new XMLHttpRequest(); 
	
	xhr1.open('GET', `data1.json`);
	xhr1.send();
	xhr1.onreadystatechange = function() {
		if (xhr1.readyState == 4 && xhr1.status == 200) {
			
			data1 = JSON.parse(xhr1.responseText);
		}  
	}
}



but.addEventListener('click',function(){//событие для кнопки but, которое фильтрует вывод заведений, исходя из выбраных select'ов, на экран 
	if (selectad.value != "null")//---------------------------------------------фильтры для финального json файла
	{
		result = data.filter(data=> data.admArea == selectad.value);
		
		if(selectty.value != "null")
		{
			result1 = result.filter(result=> result.typeObject == selectty.value); 
			if(selectdi.value != "null")
			{
				result2 = result1.filter(result1=> result1.district == selectdi.value);	
				if(selectso.value == "true")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == true);					 
				}
				if(selectso.value == "false")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == false);					 
				}
				if(selectso.value == "null")
				{
					finalres=result2;
				}				 
			}
			else
			{
				result2=result1;
				if(selectso.value == "true")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == true);					 
				}
				if(selectso.value == "false")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == false);					 
				}
				if(selectso.value == "null")
				{
					finalres=result2;
				}
			}
		}
		else {
			result1=result;
			if(selectdi.value != "null")
			{
				result2 = result1.filter(result1=> result1.district == selectdi.value);			
				if(selectso.value == "true")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == true);					 
				}
				if(selectso.value == "false")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == false);					 
				}
				if(selectso.value == "null")
				{
					finalres=result2;
				}				 
			}
			else
			{
				result2=result1;
				if(selectso.value == "true")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == true);					 
				}
				if(selectso.value == "false")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == false);					 
				}
				if(selectso.value == "null")
				{
					finalres=result2;
				}
			}
		}
	}
	
	
	
	
	
	
	else{
		result=data;
		if(selectty.value != "null")
		{
			result1 = result.filter(result=> result.typeObject == selectty.value); 
			if(selectdi.value != "null")
			{
				result2 = result1.filter(result1=> result1.district == selectdi.value);
				
				if(selectso.value == "true")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == true);					 
				}
				if(selectso.value == "false")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == false);					 
				}
				if(selectso.value == "null")
				{
					finalres=result2;
				}
			}
			else
			{
				result2=result1;
				
				if(selectso.value == "true")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == true);					 
				}
				if(selectso.value == "false")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == false);					 
				}
				if(selectso.value == "null")
				{
					finalres=result2;
				}
				
			}
		}
		else {
			result1=result;
			if(selectdi.value != "null")
			{
				result2 = result1.filter(result1=> result1.district == selectdi.value);	
				if(selectso.value == "true")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == true);					 
				}
				if(selectso.value == "false")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == false);					 
				}
				if(selectso.value == "null")
				{
					finalres=result2;
				}				 
			}
			else
			{
				result2=result1;
				if(selectso.value == "true")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == true);					 
				}
				if(selectso.value == "false")
				{
					finalres= result2.filter(result2=> result2.socialPrivileges == false);					 
				}
				if(selectso.value == "null")
				{
					finalres=result2;
				}
			}
		}
		
	}//-----------------------------------------------------------------------------------------
	console.log(finalres);
	let countOfItems = Math.ceil(finalres.length / notesOnPage);
pagination.innerHTML="";
for (let i=1; i<= countOfItems;i++)
{
	let li = document.createElement('li');
	li.className="border border-primary"
	li.id="li"
	li.innerHTML = i;
	pagination.appendChild(li);
}
let items = document.querySelectorAll('#pagination li');
for (let item of items) {
	item.addEventListener('click', function(){
		let pageNum =+this.innerHTML;
		
		

	console.log(finalres);
	let start =(pageNum - 1)*notesOnPage;
	let end = start + notesOnPage;
	let notes = finalres.slice(start, end);//создание json файла, который содержит первые 10 пунктов от финального json файла
	Table.innerHTML="";
	for (let note of notes){//цикл для вывода первых 10-ти пунктов на экран----------------------
		let row = document.createElement('div');
		row.className="row border border-dark rounded m-3";
		Table.appendChild(row);
		let col;	
		col=document.createElement('div');
		col.className="col border-right";
		col.innerHTML=note.name;
		row.appendChild(col);
		col=document.createElement('div');
		col.className="col border-right";
		col.innerHTML=note.typeObject;
		row.appendChild(col);
		col=document.createElement('div');
		col.className="col border-right";
		col.innerHTML=note.address;
		row.appendChild(col);
		col=document.createElement('div');
		col.className="col border-right  text-center p-3";
		row.appendChild(col);
		buttonn=document.createElement('button');
		buttonn.id=note.id;
		buttonn.className="btn btn-outline-secondary 1";
		buttonn.innerHTML="Выбрать";
		col.appendChild(buttonn);//--------------------------------
}
	});
}
});







